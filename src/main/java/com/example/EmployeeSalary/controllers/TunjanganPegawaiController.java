package com.example.EmployeeSalary.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeSalary.exceptions.ResourceNotFoundException;
import com.example.EmployeeSalary.models.TunjanganPegawai;
import com.example.EmployeeSalary.models.Posisi;
import com.example.EmployeeSalary.models.Tingkatan;
import com.example.EmployeeSalary.models.dto.TunjanganPegawaiDTO;
import com.example.EmployeeSalary.repositories.TunjanganPegawaiRepository;

@RestController
@RequestMapping("/api")
public class TunjanganPegawaiController {
	
	@Autowired
	TunjanganPegawaiRepository tunjanganPegawaiRepository;
	
	// Get All TunjanganPegawai DTO Mapper
	@GetMapping("/tunjanganPegawai/read")
	public HashMap<String, Object> getAllDataMapper() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		//Ambil semua data dari TunjanganPegawai dengan menggunakan method findAll() dari repository
		ArrayList<TunjanganPegawai> listTunjanganPegawaiEntity = (ArrayList<TunjanganPegawai>) tunjanganPegawaiRepository.findAll();
		
		//Buat sebuah arrayList DTO
		ArrayList<TunjanganPegawaiDTO> listTunjanganPegawaiDTO = new ArrayList<TunjanganPegawaiDTO>();
		ModelMapper modelMapper = new ModelMapper();
		for(TunjanganPegawai tunjanganPegawai : listTunjanganPegawaiEntity) {

			//inisialsasi object paper DTO
			TunjanganPegawaiDTO tunjanganPegawaiDTO = modelMapper.map(tunjanganPegawai, TunjanganPegawaiDTO.class);
			
			//memasukan object author DTO ke arraylist
			listTunjanganPegawaiDTO.add(tunjanganPegawaiDTO);
		}
		
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Read All TunjanganPegawai Data Success");
		result.put("Data", listTunjanganPegawaiDTO);
						
		return result;
	}
	
	// Create a new TunjanganPegawai DTO Mapper
	@PostMapping("/tunjanganPegawai/create")
	public HashMap<String, Object> createTunjanganPegawaiDTOMapper(@Valid @RequestBody TunjanganPegawaiDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		TunjanganPegawai tunjanganPegawaiEntity = modelMapper.map(body, TunjanganPegawai.class);
		
		//ini proses save data ke database
		tunjanganPegawaiRepository.save(tunjanganPegawaiEntity);		
		body.setIdTunjanganPegawai(tunjanganPegawaiEntity.getIdTunjanganPegawai());
				
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Create New TunjanganPegawai Success");
		result.put("Data", body);
				
		return result;
	}
	
	// Update TunjanganPegawai DTO Mapper
	@PutMapping("/tunjanganPegawai/update/{id}")
	public HashMap<String, Object> updateTunjanganPegawaiDTOMapper(@PathVariable(value = "id") Integer idTunjanganPegawai, 
			@Valid @RequestBody TunjanganPegawaiDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		TunjanganPegawai tunjanganPegawaiEntity = tunjanganPegawaiRepository.findById(idTunjanganPegawai)
	            .orElseThrow(() -> new ResourceNotFoundException("TunjanganPegawai", "id", idTunjanganPegawai));
		
		ModelMapper modelMapper = new ModelMapper();
		
		tunjanganPegawaiEntity = modelMapper.map(body, TunjanganPegawai.class);
		tunjanganPegawaiEntity.setIdTunjanganPegawai(idTunjanganPegawai);
		
		
		//ini proses save data ke database
		tunjanganPegawaiRepository.save(tunjanganPegawaiEntity);		
		body.setIdTunjanganPegawai(tunjanganPegawaiEntity.getIdTunjanganPegawai());
				
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Update TunjanganPegawai Success");
		result.put("Data", body);
				
		return result;
	}
	
	// Delete a TunjanganPegawai DTO Mapper
	@DeleteMapping("/tunjanganPegawai/delete/{id}")
	public HashMap<String, Object> deleteTunjanganPegawaiDTOMapper(@PathVariable(value = "id") Integer idTunjanganPegawai, TunjanganPegawaiDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		TunjanganPegawai tunjanganPegawaiEntity = tunjanganPegawaiRepository.findById(idTunjanganPegawai)
	            .orElseThrow(() -> new ResourceNotFoundException("TunjanganPegawai", "id", idTunjanganPegawai));
		
		ModelMapper modelMapper = new ModelMapper();
		tunjanganPegawaiEntity = modelMapper.map(body, TunjanganPegawai.class);
		tunjanganPegawaiEntity.setIdTunjanganPegawai(idTunjanganPegawai);
		
		//ini proses delete data
		tunjanganPegawaiRepository.delete(tunjanganPegawaiEntity);
				
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Delete TunjanganPegawai Success");
				
		return result;
	}

}
