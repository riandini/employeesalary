package com.example.EmployeeSalary.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeSalary.exceptions.ResourceNotFoundException;
import com.example.EmployeeSalary.models.Karyawan;
import com.example.EmployeeSalary.models.dto.KaryawanDTO;
import com.example.EmployeeSalary.repositories.KaryawanRepository;

@RestController
@RequestMapping("/api")
public class KaryawanController {
	
	@Autowired
	KaryawanRepository karyawanRepository;
	
	// Get All Karyawan DTO Mapper
	@GetMapping("/karyawan/read")
	public HashMap<String, Object> getAllDataMapper() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		//Ambil semua data dari Karyawan dengan menggunakan method findAll() dari repository
		ArrayList<Karyawan> listKaryawanEntity = (ArrayList<Karyawan>) karyawanRepository.findAll();
		
		//Buat sebuah arrayList DTO
		ArrayList<KaryawanDTO> listKaryawanDTO = new ArrayList<KaryawanDTO>();
		ModelMapper modelMapper = new ModelMapper();
		for(Karyawan karyawan : listKaryawanEntity) {

			//inisialsasi object paper DTO
			KaryawanDTO karyawanDTO = modelMapper.map(karyawan, KaryawanDTO.class);
			
			//memasukan object author DTO ke arraylist
			listKaryawanDTO.add(karyawanDTO);
		}
		
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Read All Karyawan Data Success");
		result.put("Data", listKaryawanDTO);
						
		return result;
	}
	
	// Create a new Karyawan DTO Mapper
	@PostMapping("/karyawan/create")
	public HashMap<String, Object> createKaryawanDTOMapper(@Valid @RequestBody KaryawanDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Karyawan karyawanEntity = modelMapper.map(body, Karyawan.class);
		
		//ini proses save data ke database
		karyawanRepository.save(karyawanEntity);	
		body.setIdKaryawan(karyawanEntity.getIdKaryawan());
				
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Create New Karyawan Success");
		result.put("Data", body);
				
		return result;
	}
	
	// Update Karyawan DTO Mapper
	@PutMapping("/karyawan/update/{id}")
	public HashMap<String, Object> updateKaryawanDTOMapper(@PathVariable(value = "id") Integer idKaryawan, 
			@Valid @RequestBody KaryawanDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Karyawan karyawanEntity = karyawanRepository.findById(idKaryawan)
	            .orElseThrow(() -> new ResourceNotFoundException("Karyawan", "id", idKaryawan));
		
		ModelMapper modelMapper = new ModelMapper();
		karyawanEntity = modelMapper.map(body, Karyawan.class);
		karyawanEntity.setIdKaryawan(idKaryawan);
		
		//ini proses save data ke database
		karyawanRepository.save(karyawanEntity);		
		body.setIdKaryawan(karyawanEntity.getIdKaryawan());
				
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Update Karyawan Success");
		result.put("Data", body);
				
		return result;
	}
	
	// Delete a Karyawan DTO Mapper
	@DeleteMapping("/karyawan/delete/{id}")
	public HashMap<String, Object> deleteKaryawanDTOMapper(@PathVariable(value = "id") Integer idKaryawan, KaryawanDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Karyawan karyawanEntity = karyawanRepository.findById(idKaryawan)
	            .orElseThrow(() -> new ResourceNotFoundException("Karyawan", "id", idKaryawan));
		
		ModelMapper modelMapper = new ModelMapper();
		karyawanEntity = modelMapper.map(body, Karyawan.class);
		karyawanEntity.setIdKaryawan(idKaryawan);
		
		//ini proses delete data
		karyawanRepository.delete(karyawanEntity);
				
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Delete Karyawan Success");
				
		return result;
	}

}
