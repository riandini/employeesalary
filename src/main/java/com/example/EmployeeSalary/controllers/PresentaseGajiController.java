package com.example.EmployeeSalary.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeSalary.exceptions.ResourceNotFoundException;
import com.example.EmployeeSalary.models.Posisi;
import com.example.EmployeeSalary.models.PresentaseGaji;
import com.example.EmployeeSalary.models.dto.PresentaseGajiDTO;
import com.example.EmployeeSalary.repositories.PresentaseGajiRepository;

@RestController
@RequestMapping("/api")
public class PresentaseGajiController {
	
	@Autowired
	PresentaseGajiRepository presentaseGajiRepository;
	
	// Get All PresentaseGaji DTO Mapper
	@GetMapping("/presentaseGaji/read")
	public HashMap<String, Object> getAllDataMapper() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		//Ambil semua data dari PresentaseGaji dengan menggunakan method findAll() dari repository
		ArrayList<PresentaseGaji> listPresentaseGajiEntity = (ArrayList<PresentaseGaji>) presentaseGajiRepository.findAll();
		
		//Buat sebuah arrayList DTO
		ArrayList<PresentaseGajiDTO> listPresentaseGajiDTO = new ArrayList<PresentaseGajiDTO>();
		ModelMapper modelMapper = new ModelMapper();
		for(PresentaseGaji presentaseGaji : listPresentaseGajiEntity) {

			//inisialsasi object paper DTO
			PresentaseGajiDTO presentaseGajiDTO = modelMapper.map(presentaseGaji, PresentaseGajiDTO.class);
			
			//memasukan object author DTO ke arraylist
			listPresentaseGajiDTO.add(presentaseGajiDTO);
		}
		
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Read All PresentaseGaji Data Success");
		result.put("Data", listPresentaseGajiDTO);
						
		return result;
	}
	
	// Create a new PresentaseGaji DTO Mapper
	@PostMapping("/presentaseGaji/create")
	public HashMap<String, Object> createPresentaseGajiDTOMapper(@Valid @RequestBody PresentaseGajiDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		PresentaseGaji presentaseGajiEntity = modelMapper.map(body, PresentaseGaji.class);
		
		Posisi posisiEntity = modelMapper.map(body, Posisi.class);
		posisiEntity.setIdPosisi(body.getPosisi().getIdPosisi());
		
		//ini proses save data ke database
		presentaseGajiRepository.save(presentaseGajiEntity);	
		body.setIdPresentaseGaji(presentaseGajiEntity.getIdPresentaseGaji());
				
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Create New PresentaseGaji Success");
		result.put("Data", body);
				
		return result;
	}
	
	// Update PresentaseGaji DTO Mapper
	@PutMapping("/presentaseGaji/update/{id}")
	public HashMap<String, Object> updatePresentaseGajiDTOMapper(@PathVariable(value = "id") Integer idPresentaseGaji, 
			@Valid @RequestBody PresentaseGajiDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		PresentaseGaji presentaseGajiEntity = presentaseGajiRepository.findById(idPresentaseGaji)
	            .orElseThrow(() -> new ResourceNotFoundException("PresentaseGaji", "id", idPresentaseGaji));
		
		ModelMapper modelMapper = new ModelMapper();
		
		presentaseGajiEntity = modelMapper.map(body, PresentaseGaji.class);
		presentaseGajiEntity.setIdPresentaseGaji(idPresentaseGaji);
		
		//ini proses save data ke database
		presentaseGajiRepository.save(presentaseGajiEntity);		
		body.setIdPresentaseGaji(presentaseGajiEntity.getIdPresentaseGaji());
				
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Update PresentaseGaji Success");
		result.put("Data", body);
				
		return result;
	}
	
	// Delete a PresentaseGaji DTO Mapper
	@DeleteMapping("/presentaseGaji/delete/{id}")
	public HashMap<String, Object> deletePresentaseGajiDTOMapper(@PathVariable(value = "id") Integer idPresentaseGaji, PresentaseGajiDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		PresentaseGaji presentaseGajiEntity = presentaseGajiRepository.findById(idPresentaseGaji)
	            .orElseThrow(() -> new ResourceNotFoundException("PresentaseGaji", "id", idPresentaseGaji));
		
		ModelMapper modelMapper = new ModelMapper();
		presentaseGajiEntity = modelMapper.map(body, PresentaseGaji.class);
		presentaseGajiEntity.setIdPresentaseGaji(idPresentaseGaji);
		
		//ini proses delete data
		presentaseGajiRepository.delete(presentaseGajiEntity);
				
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Delete PresentaseGaji Success");
				
		return result;
	}

}
