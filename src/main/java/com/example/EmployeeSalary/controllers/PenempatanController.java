package com.example.EmployeeSalary.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeSalary.exceptions.ResourceNotFoundException;
import com.example.EmployeeSalary.models.Penempatan;
import com.example.EmployeeSalary.models.dto.PenempatanDTO;
import com.example.EmployeeSalary.repositories.PenempatanRepository;

@RestController
@RequestMapping("/api")
public class PenempatanController {
	
	@Autowired
	PenempatanRepository penempatanRepository;
	
	// Get All Penempatan DTO Mapper
	@GetMapping("/penempatan/read")
	public HashMap<String, Object> getAllDataMapper() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		//Ambil semua data dari Penempatan dengan menggunakan method findAll() dari repository
		ArrayList<Penempatan> listPenempatanEntity = (ArrayList<Penempatan>) penempatanRepository.findAll();
		
		//Buat sebuah arrayList DTO
		ArrayList<PenempatanDTO> listPenempatanDTO = new ArrayList<PenempatanDTO>();
		ModelMapper modelMapper = new ModelMapper();
		for(Penempatan penempatan : listPenempatanEntity) {

			//inisialsasi object paper DTO
			PenempatanDTO penempatanDTO = modelMapper.map(penempatan, PenempatanDTO.class);
			
			//memasukan object author DTO ke arraylist
			listPenempatanDTO.add(penempatanDTO);
		}
		
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Read All Penempatan Data Success");
		result.put("Data", listPenempatanDTO);
						
		return result;
	}
	
	// Create a new Penempatan DTO Mapper
	@PostMapping("/penempatan/create")
	public HashMap<String, Object> createPenempatanDTOMapper(@Valid @RequestBody PenempatanDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Penempatan penempatanEntity = modelMapper.map(body, Penempatan.class);
		
		//ini proses save data ke database
		penempatanRepository.save(penempatanEntity);	
		body.setIdPenempatan(penempatanEntity.getIdPenempatan());
				
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Create New Penempatan Success");
		result.put("Data", body);
				
		return result;
	}
	
	// Update Penempatan DTO Mapper
	@PutMapping("/penempatan/update/{id}")
	public HashMap<String, Object> updatePenempatanDTOMapper(@PathVariable(value = "id") Integer idPenempatan, 
			@Valid @RequestBody PenempatanDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Penempatan penempatanEntity = penempatanRepository.findById(idPenempatan)
	            .orElseThrow(() -> new ResourceNotFoundException("Penempatan", "id", idPenempatan));
		
		ModelMapper modelMapper = new ModelMapper();
		penempatanEntity = modelMapper.map(body, Penempatan.class);
		penempatanEntity.setIdPenempatan(idPenempatan);
		
		//ini proses save data ke database
		penempatanRepository.save(penempatanEntity);
		body.setIdPenempatan(penempatanEntity.getIdPenempatan());
				
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Update Penempatan Success");
		result.put("Data", body);
				
		return result;
	}
	
	// Delete a Penempatan DTO Mapper
	@DeleteMapping("/penempatan/delete/{id}")
	public HashMap<String, Object> deletePenempatanDTOMapper(@PathVariable(value = "id") Integer idPenempatan, PenempatanDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Penempatan penempatanEntity = penempatanRepository.findById(idPenempatan)
	            .orElseThrow(() -> new ResourceNotFoundException("Penempatan", "id", idPenempatan));
		
		ModelMapper modelMapper = new ModelMapper();
		penempatanEntity = modelMapper.map(body, Penempatan.class);
		penempatanEntity.setIdPenempatan(idPenempatan);
		
		//ini proses delete data
		penempatanRepository.delete(penempatanEntity);
				
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Delete Penempatan Success");
				
		return result;
	}

}
