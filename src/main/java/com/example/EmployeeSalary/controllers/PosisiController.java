package com.example.EmployeeSalary.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeSalary.exceptions.ResourceNotFoundException;
import com.example.EmployeeSalary.models.Posisi;
import com.example.EmployeeSalary.models.dto.PosisiDTO;
import com.example.EmployeeSalary.repositories.PosisiRepository;

@RestController
@RequestMapping("/api")
public class PosisiController {
	
	@Autowired
	PosisiRepository posisiRepository;
	
	// Get All Posisi DTO Mapper
	@GetMapping("/posisi/read")
	public HashMap<String, Object> getAllDataMapper() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		//Ambil semua data dari Posisi dengan menggunakan method findAll() dari repository
		ArrayList<Posisi> listPosisiEntity = (ArrayList<Posisi>) posisiRepository.findAll();
		
		//Buat sebuah arrayList DTO
		ArrayList<PosisiDTO> listPosisiDTO = new ArrayList<PosisiDTO>();
		ModelMapper modelMapper = new ModelMapper();
		for(Posisi posisi : listPosisiEntity) {

			//inisialsasi object paper DTO
			PosisiDTO posisiDTO = modelMapper.map(posisi, PosisiDTO.class);
			
			//memasukan object author DTO ke arraylist
			listPosisiDTO.add(posisiDTO);
		}
		
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Read All Posisi Data Success");
		result.put("Data", listPosisiDTO);
						
		return result;
	}
	
	// Create a new Posisi DTO Mapper
	@PostMapping("/posisi/create")
	public HashMap<String, Object> createPosisiDTOMapper(@Valid @RequestBody PosisiDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Posisi posisiEntity = modelMapper.map(body, Posisi.class);
		
		//ini proses save data ke database
		posisiRepository.save(posisiEntity);		
		body.setIdPosisi(posisiEntity.getIdPosisi());
				
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Create New Posisi Success");
		result.put("Data", body);
				
		return result;
	}
	
	// Update Posisi DTO Mapper
	@PutMapping("/posisi/update/{id}")
	public HashMap<String, Object> updatePosisiDTOMapper(@PathVariable(value = "id") Integer idPosisi, 
			@Valid @RequestBody PosisiDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Posisi posisiEntity = posisiRepository.findById(idPosisi)
	            .orElseThrow(() -> new ResourceNotFoundException("Posisi", "id", idPosisi));
		
		ModelMapper modelMapper = new ModelMapper();
		posisiEntity = modelMapper.map(body, Posisi.class);
		posisiEntity.setIdPosisi(idPosisi);
		
		//ini proses save data ke database
		posisiRepository.save(posisiEntity);		
		body.setIdPosisi(posisiEntity.getIdPosisi());
				
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Update Posisi Success");
		result.put("Data", body);
				
		return result;
	}
	
	// Delete a Posisi DTO Mapper
	@DeleteMapping("/posisi/delete/{id}")
	public HashMap<String, Object> deletePosisiDTOMapper(@PathVariable(value = "id") Integer idPosisi, PosisiDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Posisi posisiEntity = posisiRepository.findById(idPosisi)
	            .orElseThrow(() -> new ResourceNotFoundException("Posisi", "id", idPosisi));
		
		ModelMapper modelMapper = new ModelMapper();
		posisiEntity = modelMapper.map(body, Posisi.class);
		posisiEntity.setIdPosisi(idPosisi);
		
		//ini proses delete data
		posisiRepository.delete(posisiEntity);
				
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Delete Posisi Success");
				
		return result;
	}

}
