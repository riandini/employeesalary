package com.example.EmployeeSalary.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeSalary.exceptions.ResourceNotFoundException;
import com.example.EmployeeSalary.models.Tingkatan;
import com.example.EmployeeSalary.models.dto.TingkatanDTO;
import com.example.EmployeeSalary.repositories.TingkatanRepository;

@RestController
@RequestMapping("/api")
public class TingkatanController {
	
	@Autowired
	TingkatanRepository tingkatanRepository;
	
	// Get All Tingkatan DTO Mapper
	@GetMapping("/tingkatan/read")
	public HashMap<String, Object> getAllDataMapper() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		//Ambil semua data dari Tingkatan dengan menggunakan method findAll() dari repository
		ArrayList<Tingkatan> listTingkatanEntity = (ArrayList<Tingkatan>) tingkatanRepository.findAll();
		
		//Buat sebuah arrayList DTO
		ArrayList<TingkatanDTO> listTingkatanDTO = new ArrayList<TingkatanDTO>();
		ModelMapper modelMapper = new ModelMapper();
		for(Tingkatan tingkatan : listTingkatanEntity) {

			//inisialsasi object paper DTO
			TingkatanDTO tingkatanDTO = modelMapper.map(tingkatan, TingkatanDTO.class);
			
			//memasukan object author DTO ke arraylist
			listTingkatanDTO.add(tingkatanDTO);
		}
		
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Read All Tingkatan Data Success");
		result.put("Data", listTingkatanDTO);
						
		return result;
	}
	
	// Create a new Tingkatan DTO Mapper
	@PostMapping("/tingkatan/create")
	public HashMap<String, Object> createTingkatanDTOMapper(@Valid @RequestBody TingkatanDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Tingkatan tingkatanEntity = modelMapper.map(body, Tingkatan.class);
		
		//ini proses save data ke database
		tingkatanRepository.save(tingkatanEntity);		
		body.setIdTingkatan(tingkatanEntity.getIdTingkatan());
				
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Create New Tingkatan Success");
		result.put("Data", body);
				
		return result;
	}
	
	// Update Tingkatan DTO Mapper
	@PutMapping("/tingkatan/update/{id}")
	public HashMap<String, Object> updateTingkatanDTOMapper(@PathVariable(value = "id") Integer idTingkatan, 
			@Valid @RequestBody TingkatanDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Tingkatan tingkatanEntity = tingkatanRepository.findById(idTingkatan)
	            .orElseThrow(() -> new ResourceNotFoundException("Tingkatan", "id", idTingkatan));
		
		ModelMapper modelMapper = new ModelMapper();
		tingkatanEntity = modelMapper.map(body, Tingkatan.class);
		tingkatanEntity.setIdTingkatan(idTingkatan);
		
		//ini proses save data ke database
		tingkatanRepository.save(tingkatanEntity);		
		body.setIdTingkatan(tingkatanEntity.getIdTingkatan());
				
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Update Tingkatan Success");
		result.put("Data", body);
				
		return result;
	}
	
	// Delete a Tingkatan DTO Mapper
	@DeleteMapping("/tingkatan/delete/{id}")
	public HashMap<String, Object> deleteTingkatanDTOMapper(@PathVariable(value = "id") Integer idTingkatan, TingkatanDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Tingkatan tingkatanEntity = tingkatanRepository.findById(idTingkatan)
	            .orElseThrow(() -> new ResourceNotFoundException("Tingkatan", "id", idTingkatan));
		
		ModelMapper modelMapper = new ModelMapper();
		tingkatanEntity = modelMapper.map(body, Tingkatan.class);
		tingkatanEntity.setIdTingkatan(idTingkatan);
		
		//ini proses delete data
		tingkatanRepository.delete(tingkatanEntity);
				
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Delete Tingkatan Success");
				
		return result;
	}

}
