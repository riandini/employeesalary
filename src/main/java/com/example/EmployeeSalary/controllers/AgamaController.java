package com.example.EmployeeSalary.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeSalary.models.Agama;
import com.example.EmployeeSalary.models.dto.AgamaDTO;
import com.example.EmployeeSalary.repositories.AgamaRepository;
import com.example.EmployeeSalary.exceptions.ResourceNotFoundException;

@RestController
@RequestMapping("/api")
public class AgamaController {
	
	@Autowired
    AgamaRepository agamaRepository;
	
	// Get All Agama DTO Mapper
	@GetMapping("/agama/read")
	public HashMap<String, Object> getAllDataMapper() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		//Ambil semua data dari Agama dengan menggunakan method findAll() dari repository
		ArrayList<Agama> listAgamaEntity = (ArrayList<Agama>) agamaRepository.findAll();
		
		//Buat sebuah arrayList DTO
		ArrayList<AgamaDTO> listAgamaDTO = new ArrayList<AgamaDTO>();
		ModelMapper modelMapper = new ModelMapper();
		for(Agama agama : listAgamaEntity) {

			//inisialsasi object paper DTO
			AgamaDTO agamaDTO = modelMapper.map(agama, AgamaDTO.class);
			
			//memasukan object author DTO ke arraylist
			listAgamaDTO.add(agamaDTO);
		}
		
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Read All Agama Data Success");
		result.put("Data", listAgamaDTO);
						
		return result;
	}
	
	// Create a new Agama DTO Mapper
	@PostMapping("/agama/create")
	public HashMap<String, Object> createAgamaDTOMapper(@Valid @RequestBody AgamaDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Agama agamaEntity = modelMapper.map(body, Agama.class);
		
		//ini proses save data ke database
		agamaRepository.save(agamaEntity);		
		body.setIdAgama(agamaEntity.getIdAgama());
				
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Create New Agama Success");
		result.put("Data", body);
				
		return result;
	}
	
	// Update Agama DTO Mapper
	@PutMapping("/agama/update/{id}")
	public HashMap<String, Object> updateAgamaDTOMapper(@PathVariable(value = "id") Integer idAgama, 
			@Valid @RequestBody AgamaDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Agama agamaEntity = agamaRepository.findById(idAgama)
	            .orElseThrow(() -> new ResourceNotFoundException("Agama", "id", idAgama));
		
		ModelMapper modelMapper = new ModelMapper();
		agamaEntity = modelMapper.map(body, Agama.class);
		agamaEntity.setIdAgama(idAgama);
		
		//ini proses save data ke database
		agamaRepository.save(agamaEntity);		
		body.setIdAgama(agamaEntity.getIdAgama());
				
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Update Agama Success");
		result.put("Data", body);
				
		return result;
	}
	
	// Delete a Agama DTO Mapper
	@DeleteMapping("/agama/delete/{id}")
	public HashMap<String, Object> deleteAgamaDTOMapper(@PathVariable(value = "id") Integer idAgama, AgamaDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Agama agamaEntity = agamaRepository.findById(idAgama)
	            .orElseThrow(() -> new ResourceNotFoundException("Agama", "id", idAgama));
		
		ModelMapper modelMapper = new ModelMapper();
		agamaEntity = modelMapper.map(body, Agama.class);
		agamaEntity.setIdAgama(idAgama);
		
		//ini proses delete data
		agamaRepository.delete(agamaEntity);
				
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Delete Agama Success");
				
		return result;
	}

}
