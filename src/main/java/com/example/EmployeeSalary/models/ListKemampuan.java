package com.example.EmployeeSalary.models;
// Generated May 11, 2020 9:10:48 AM by Hibernate Tools 4.3.1.Final

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * ListKemampuan generated by hbm2java
 */
@Entity
@Table(name = "list_kemampuan", schema = "public")
public class ListKemampuan implements java.io.Serializable {

	private int idListKemampuan;
	private Karyawan karyawan;
	private Kemampuan kemampuan;
	private Integer nilaiKemampuan;

	public ListKemampuan() {
	}

	public ListKemampuan(int idListKemampuan) {
		this.idListKemampuan = idListKemampuan;
	}

	public ListKemampuan(int idListKemampuan, Karyawan karyawan, Kemampuan kemampuan, Integer nilaiKemampuan) {
		this.idListKemampuan = idListKemampuan;
		this.karyawan = karyawan;
		this.kemampuan = kemampuan;
		this.nilaiKemampuan = nilaiKemampuan;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_list_kemampuan_id_list_kemampuan_seq")
	@SequenceGenerator(name = "generator_list_kemampuan_id_list_kemampuan_seq", sequenceName = "list_kemampuan_id_list_kemampuan_seq", schema = "public", allocationSize = 1)
	@Column(name = "id_list_kemampuan", unique = true, nullable = false)
	public int getIdListKemampuan() {
		return this.idListKemampuan;
	}

	public void setIdListKemampuan(int idListKemampuan) {
		this.idListKemampuan = idListKemampuan;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_karyawan")
	public Karyawan getKaryawan() {
		return this.karyawan;
	}

	public void setKaryawan(Karyawan karyawan) {
		this.karyawan = karyawan;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_kemampuan")
	public Kemampuan getKemampuan() {
		return this.kemampuan;
	}

	public void setKemampuan(Kemampuan kemampuan) {
		this.kemampuan = kemampuan;
	}

	@Column(name = "nilai_kemampuan")
	public Integer getNilaiKemampuan() {
		return this.nilaiKemampuan;
	}

	public void setNilaiKemampuan(Integer nilaiKemampuan) {
		this.nilaiKemampuan = nilaiKemampuan;
	}

}
