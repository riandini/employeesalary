package com.example.EmployeeSalary.models.dto;

public class KemampuanDTO {
	
	private int idKemampuan;
	private KategoriKemampuanDTO kategoriKemampuan;
	private String namaKemampuan;
	
	public KemampuanDTO() {
		super();
	}

	public KemampuanDTO(int idKemampuan, KategoriKemampuanDTO kategoriKemampuan, String namaKemampuan) {
		super();
		this.idKemampuan = idKemampuan;
		this.kategoriKemampuan = kategoriKemampuan;
		this.namaKemampuan = namaKemampuan;
	}

	public int getIdKemampuan() {
		return idKemampuan;
	}

	public void setIdKemampuan(int idKemampuan) {
		this.idKemampuan = idKemampuan;
	}

	public KategoriKemampuanDTO getKategoriKemampuan() {
		return kategoriKemampuan;
	}

	public void setKategoriKemampuan(KategoriKemampuanDTO kategoriKemampuan) {
		this.kategoriKemampuan = kategoriKemampuan;
	}

	public String getNamaKemampuan() {
		return namaKemampuan;
	}

	public void setNamaKemampuan(String namaKemampuan) {
		this.namaKemampuan = namaKemampuan;
	}

}
