package com.example.EmployeeSalary.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.EmployeeSalary.models.Posisi;

@Repository
public interface PosisiRepository extends JpaRepository<Posisi, Integer> {

}
